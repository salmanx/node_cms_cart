const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const session = require('express-session');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const fileUpload = require('express-fileupload');
const passport = require('passport');
const passportConfig = require('./app/config/passport');

const routes = require("./app/routes");

mongoose.connect('mongodb://localhost/cmscart');
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("mongodb connected!");
});

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'app/assets')));
app.use(express.static(path.join(__dirname, 'public')));

// body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// Express Validator middleware
app.use(expressValidator({
  errorFormatter: function (param, msg, value) {
      var namespace = param.split('.')
              , root = namespace.shift()
              , formParam = root;

      while (namespace.length) {
          formParam += '[' + namespace.shift() + ']';
      }
      return {
          param: formParam,
          msg: msg,
          value: value
      };
  },
  customValidators: {
    isImage: (value, fileName) =>{
      let extension = (path.extname(fileName)).toLowerCase();

      switch(extension){
        case ".jpg" :
          return ".jpg";
        case ".png" :
          return ".png";
        case ".jpeg" :
          return ".jpeg";  
        case " " :
          return ".jpg"; 
        default:
          return false;                                     
      }
    }
  }

}));
// session middleware
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

// flash middleware
app.use(flash());
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.errors = req.flash('errors');
  next();
});

// fileupload middleware
app.use(fileUpload());

// get all pages for header
let Page = require("./app/models/page.model");
Page.find({}).sort({sorting: 1}).exec((err, pages) => {
  if(err){
    console.log(err);
  } else {
    app.locals.pages = pages;
  }
});

// get all categories in all pages
let Category = require('./app/models/category.model');
Category.find((err, cats) => {
  if(err){
    console.log(err);
  } else {
    app.locals.categories = cats;
  }
});

// passport config
passportConfig(passport);

// passport middleware
app.use(passport.initialize());
app.use(passport.session());

// get products in cart
app.get('*', (req, res, next) => {
  res.locals.cart = req.session.cart;
  res.locals.user = req.user || null;
  res.locals.isLoggedIn = req.isAuthenticated();
  next();
});

// application routes
routes(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(5000, () => console.log('Example app listening on port 5000!'))

module.exports = app;