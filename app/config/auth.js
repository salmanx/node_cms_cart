exports.isUser = (req, res, next) => {

    if(req.isAuthenticated()){
        next();
    } else {
        req.flash("error_msg", "Please login");
        res.redirect('/users/sign_in');
    }

}

exports.isAdminUser = (req, res, next) => {

    if(req.isAuthenticated() && res.locals.user.admin == 1){
        next();
    } else {
        req.flash("error_msg", "Please login as asmin");
        res.redirect('/users/sign_in');
    }

}
