const Category = require('../../models/category.model');

module.exports = {

    index: (req, res) => {
      Category.find((err, categories) => {
        if(err)
            return console.log(err);
        
        res.render('admin/categories/index', { 
            title: 'Express Ecommerce App',
            categories: categories  
        });        
      });      
    },

    new: (req, res) => {
        let title = "";

        res.render('admin/categories/new', {
          title: title,
        });
    },

    create: (req, res) => {
      req.checkBody("title", "Title must have a value").notEmpty();

      let title = req.body.title;
      let slug = title.replace(/\s+/g, '-').toLowerCase();

      let errors = req.validationErrors();
      
      if(errors){
        res.render('admin/categories/new', {
          errors: errors,
          title: title
        });
      } else {
        Category.findOne({slug: slug}, (err, category) => {
          if(category){
            req.flash('error_msg', "Title is already taken. Please try with another one.");
            res.render('admin/categories/new', {
              title: title 
            });               
          } else {
            let category = new Category({
              title: title,
              slug: slug
            });

            category.save((err) => {
              if(err) 
                return console.log(err);

              Category.find((err, cats) => {
                if(err){
                  console.log(err);
                } else {
                  req.app.locals.categories = cats;
                }
              });

              req.flash('success_msg', "Category is created successfully");
              res.redirect('/admin/categories');
            });
          }
        });
      }
    },
    
    edit: (req, res) => {
        Category.findById(req.params.id, (err, category) => {
        if(err)
          return console.log(err)

        res.render('admin/categories/edit', {
          title: category.title,
          id: category.id
        });
      });  
    },

    update: (req, res) => {
      req.checkBody("title", "Title must have a value").notEmpty();

      let title = req.body.title;
      let slug = title.replace(/\s+/g, '-').toLowerCase();
      let id = req.body.id;

      let errors = req.validationErrors();
      
      if(errors){
        res.render('admin/categories/edit', {
          errors: errors,
          title: title,
          id: id 
        });
      } else {
        Category.findOne({slug: slug, _id: {'$ne':id}}, (err, category) => {
          if(category){
            req.flash('error_msg', "Category is already taken. Please try with another one.");
            res.render('admin/categories/edit', {
              title: title,
              id: id
            });               
          } else {
            Category.findById(id, (err, category) => {
              if(err)
                return console.log(err);
              
              category.title = title;
              category.slug = slug;
              
              category.save((err) => {
                if(err) 
                  return console.log(err);

                Category.find((err, cats) => {
                  if(err){
                    console.log(err);
                  } else {
                    req.app.locals.categories = cats;
                  }
                });                  

                req.flash('success_msg', "category is updated successfully");
                res.redirect('/admin/categories/edit/' + id);
              });
            });
          }
        });
      }
    },

    delete: (req, res) => {
        Category.findByIdAndRemove(req.params.id, (err) => {
        if(err)
          return console.log(err);

        Category.find((err, cats) => {
          if(err){
            console.log(err);
          } else {
            req.app.locals.categories = cats;
          }
        });
          
        req.flash('success_msg', "category is deleted successfully");
        res.redirect('/admin/categories'); 
      })
    }
};