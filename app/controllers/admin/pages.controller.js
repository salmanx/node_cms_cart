const Page = require('../../models/page.model');



function sortPages(ids, callback){
  let count = 0;

  for(let i=0; i<ids.length; i++){
    let id = ids[i];
    count++;
    ((count) => {
      Page.findById(id, (err, page) => {
        page.sorting = count;
        page.save((err) => {
          if(err)
            return console.log(err); 
            
          ++count;
          if(count >= ids.length){
            callback();
          }  
        });  
      });
    })(count);
  }
};

module.exports = {

    index: (req, res) => {
      Page.find({}).sort({sorting: 1}).exec((err, pages) => {
        if(!err){
          res.render('admin/pages/index', { 
            title: 'Express Ecommerce App',
            pages: pages  
          });
        }
      });      
    },

    new: (req, res) => {
        let title = "";
        let slug = "";
        let content = "";

        res.render('admin/pages/new', {
          title: title,
          slug: slug,
          content: content
        });
    },

    create: (req, res) => {
      req.checkBody("title", "Title must have a value").notEmpty();
      req.checkBody("content", "Content must have a value").notEmpty();

      let title = req.body.title;
      let slug = req.body.slug.replace(/\s+/g, '-').toLowerCase();
      if(slug == "") slug = title.replace(/\s+/g, '-').toLowerCase();
      let content = req.body.content;

      let errors = req.validationErrors();
      
      if(errors){
        res.render('admin/pages/new', {
          errors: errors,
          title: title,
          slug: slug,
          content: content
        });
      } else {
        Page.findOne({slug: slug}, (err, page) => {
          if(page){
            req.flash('error_msg', "Slug is already taken. Please try with another one.");
            res.render('admin/pages/new', {
              title: title,
              slug: slug,
              content: content
            });               
          } else {
            let page = new Page({
              title: title,
              slug: slug,
              content: content,
              sorting: 0
            });

            page.save((err) => {
              if(err) 
                return console.log(err);

              Page.find({}).sort({sorting: 1}).exec((err, pages) => {
                if(err){
                  console.log(err);
                } else {
                  req.app.locals.pages = pages;
                }
              });          
                
              req.flash('success_msg', "Page is created successfully");
              res.redirect('/admin/pages');
            });
          }
        });
      }
    },
    
    edit: (req, res) => {
      Page.findById(req.params.id, (err, page) => {
        if(err)
          return console.log(err)

        res.render('admin/pages/edit', {
          title: page.title,
          slug: page.slug,
          content: page.content,
          id: page._id
        });
      });  
    },

    update: (req, res) => {
      req.checkBody("title", "Title must have a value").notEmpty();
      req.checkBody("content", "Content must have a value").notEmpty();

      let title = req.body.title;
      let slug = req.body.slug.replace(/\s+/g, '-').toLowerCase();
      if(slug == "") slug = title.replace(/\s+/g, '-').toLowerCase();
      let content = req.body.content;
      let id = req.body.id;

      let errors = req.validationErrors();
      
      if(errors){
        res.render('admin/pages/edit', {
          errors: errors,
          title: title,
          slug: slug,
          content: content,
          id: id 
        });
      } else {
        Page.findOne({slug: slug, _id: {'$ne':id}}, (err, page) => {
          if(page){
            req.flash('error_msg', "Slug is already taken. Please try with another one.");
            res.render('admin/pages/edit', {
              title: title,
              slug: slug,
              content: content,
              id: id
            });               
          } else {
            Page.findById(id, (err, page) => {
              if(err)
                return console.log(err);
              
              page.title = title;
              page.slug = slug;
              page.content = content;

              page.save((err) => {
                if(err) 
                  return console.log(err);

                Page.find({}).sort({sorting: 1}).exec((err, pages) => {
                  if(err){
                    console.log(err);
                  } else {
                    req.app.locals.pages = pages;
                  }
                });                                   

                req.flash('success_msg', "Page is updated successfully");
                res.redirect('/admin/pages/edit/' + id);
              });
            });
          }
        });
      }
    },

    delete: (req, res) => {
      Page.findByIdAndRemove(req.params.id, (err) => {
        if(err)
          return console.log(err);

        Page.find({}).sort({sorting: 1}).exec((err, pages) => {
          if(err){
            console.log(err);
          } else {
            req.app.locals.pages = pages;
          }
        });          

        req.flash('success_msg', "Page is deleted successfully");
        res.redirect('/admin/pages'); 
      })
    },

    reorderPages: (req, res) => {
      let ids  = req.body['id[]'];
      sortPages(ids, () =>{
        Page.find({}).sort({sorting: 1}).exec((err, pages) => {
          if(err){
            console.log(err);
          } else {
            req.app.locals.pages = pages;
          }
        });
      });      
    } 
};
