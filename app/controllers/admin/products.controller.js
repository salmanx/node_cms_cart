const mkdirp = require('mkdirp');
const fs = require('fs-extra')
const resizeImg = require('resize-img');

const Product = require('../../models/product.model');
const Category = require('../../models/category.model');

module.exports = {

    index: (req, res) =>{
        let count;

        Product.count((err, c) => {
            count = c;
        });

        Product.find((err, products) => {
            res.render('admin/products/index', {
                products: products,
                count: count 
            });
        });
    },

    new: (req, res) => {
        let title = "";
        let price = "";
        let description = "";

        Category.find((err, categories)=> {
            res.render("admin/products/new", {
                title: title,
                price: price,
                categories: categories,
                description: description
            });
        });
    },

    create: (req, res) => {
        let imageFile = typeof req.files.image !== "undefined" ? req.files.image.name : "" ;
        
        req.checkBody("title", "The product must have a name").notEmpty();
        req.checkBody("price", "The product must have a price").isDecimal();
        req.checkBody("description", "The product must have a description").notEmpty();
        req.checkBody("image", "Product must have an image").isImage(imageFile);

        let title = req.body.title;
        let slug = req.body.title.replace(/\s+/g, '-').toLowerCase();
        let description = req.body.description; 
        let price = req.body.price;
        let category = req.body.category;
        
        let errors = req.validationErrors();
      
        if(errors){
            Category.find((err, categories)=> {
                res.render("admin/products/new", {
                    errors: errors,
                    title: title,
                    price: price,
                    categories: categories,
                    description: description
                });
            });
        } else {
          Product.findOne({slug: slug}, (err, product) => {
            if(product){
                req.flash('error_msg', "Name is already taken. Please try with another one.");
                Category.find((err, categories)=> {
                    res.render("admin/products/new", {
                        title: title,
                        price: price,
                        categories: categories,
                        description: description
                    });
                });               
            } else {
              let product = new Product({
                title: title,
                slug: slug,
                price: parseFloat(price).toFixed(2),
                description: description,
                category: category,
                image: imageFile
              });
  
              product.save((err) => {
                if(err) 
                  return console.log(err);

                mkdirp("public/product_images/" + product._id, (err) => {
                    return console.log(err);
                }); 
                
                mkdirp("public/product_images/" + product._id + "/gallery", (err) => {
                    return console.log(err);
                }); 
                
                mkdirp("public/product_images/" + product._id + "/gallery/thumbs", (err) => {
                    return console.log(err);
                }); 
                
                if(imageFile != ""){
                    let productImage = req.files.image;
                    let path = "public/product_images/" + product._id + "/" + imageFile;

                    productImage.mv(path, (err) => {
                        return console.log(err);
                    });
                }
  
                req.flash('success_msg', "Product is created successfully");
                res.redirect('/admin/products');
              });
            }
          });
        }        
    },

    edit: (req, res) => { 
        let errors;
        if(req.session.errors) errors = req.session.errors;
        req.session.errors = null;
        
        Category.find((err, categories) => {
            Product.findById(req.params.id, (err, product) => {
                if(err)
                    return console.log(err);

                let galleryDir = "public/product_images/" + product._id + "/gallery";
                let galleryImages = null;

                fs.readdir(galleryDir, (err, files) => {
                    if(err){
                        return console.log(err);
                    } else {
                        galleryImages = files;

                        res.render("admin/products/edit", {
                            id: product._id,
                            title: product.title,
                            price: product.price,
                            categories: categories,
                            description: product.description,
                            category: product.category,
                            errors: errors,
                            image: product.image,
                            galleryImages: galleryImages
                        });
                    } 
                });             
            });
        });
    },

    update: (req, res) => {
        let imageFile = typeof req.files.image !== "undefined" ? req.files.image.name : "" ;
        
        req.checkBody("title", "The product must have a name").notEmpty();
        req.checkBody("price", "The product must have a price").isDecimal();
        req.checkBody("description", "The product must have a description").notEmpty();
        // req.checkBody("image", "Product must have an image").isImage(imageFile);

        let title = req.body.title;
        let slug = req.body.title.replace(/\s+/g, '-').toLowerCase();
        let description = req.body.description; 
        let price = req.body.price;
        let category = req.body.category;
        let pimage = req.body.pimage;
        let id = req.body.id;
        
        let errors = req.validationErrors();
      
        if(errors){
            req.session.errors = errors;
            res.redirect('/admin/products/edit/' + id);
        } else {
          Product.findOne({slug: slug, _id: {'$ne': id}}, (err, product) => {
            if(product){
                req.flash('error_msg', "Title is already taken. Please try with another one.");
                res.redirect('/admin/products/edit/' + id);               
            } else {

              Product.findById(id, (err, p) => {
                if(err)
                    return console.log(err);
                
                p.title =  title;
                p.slug = slug;
                p.price =  parseFloat(price).toFixed(2);
                p.description = description;
                p.category = category;
                if(imageFile != ""){
                    p.image = imageFile; 
                }  
                
                p.save((err) => {
                    if(err)
                        return console.log(err);

                    if(imageFile != ""){
                        if(pimage != ""){
                            fs.remove("public/product_images/" + id + "/" + pimage, (err) => {
                                if(err)
                                    return console.log(err);
                            })
                        }

                        let productImage = req.files.image;
                        let path = "public/product_images/" + id + "/" + imageFile;
    
                        productImage.mv(path, (err) => {
                            console.log(err);
                        });                        
                    }                     
                });

                req.flash('success_msg', "Product is updatd successfully");
                res.redirect('/admin/products/edit/' + id);     
              });
            }
          });
        }        
    },

    delete: (req, res) => {
        let id = req.params.id;
        let imagePath = "public/product_images/" + id;

        fs.remove(imagePath, (err) => {
            if(err){
                console.log(err);
            } else {
                Product.findByIdAndRemove(id, (err) => {
                    console.log(err);
                });

                req.flash('success_msg', "Product is deleted successfully");
                res.redirect('/admin/products');                
            }
        });
    },

    uploadGalleryImages: (req, res) => {
        let galleryImage = req.files.file;
        let id = req.params.id;
        let galleryPath = "public/product_images/" + id + "/gallery/" + req.files.file.name;
        let galleryThumbsPath = "public/product_images/" + id + "/gallery/thumbs/" + req.files.file.name;
        
        galleryImage.mv(galleryPath, (err) => {
            if(err)
                return console.log(err);  
                
            resizeImg(fs.readFileSync(galleryPath), {width: 100, height: 100})
                .then((buff) => {
                    fs.writeFileSync(galleryThumbsPath, buff);
                });                
        });
        
        res.sendStatus(200);
    },

    deleteGalleryimage: (req, res) => {
        let id = req.query.id;
        let originalImage = "public/product_images/" + id + "/gallery/" + req.params.image;
        let thumbImage = "public/product_images/" + id + "/gallery/thumbs/" + req.params.image;

        fs.remove(originalImage, (err) => {
            if(err){
                console.log(err);
            } else {
                fs.remove(thumbImage, (err) => {
                    if(err){
                        console.log(err);
                    } else {
                        req.flash('success_msg', "Product image is deleted successfully");
                        res.redirect('/admin/products/edit/' + id);                        
                    }
                });
            }                
        });
    }

};