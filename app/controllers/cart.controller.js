const Product = require("../models/product.model");

module.exports = {

    addProductToCart: (req, res) => {
        let slug = req.params.slug;
        
        Product.findOne({slug: slug}, (err, product) => {
            if(err)
                return console.log(err);

            if(product){
                if(typeof req.session.cart == "undefined"){
                    req.session.cart = [];
                    req.session.cart.push({
                        title: slug,
                        qty: 1,
                        price: parseFloat(product.price).toFixed(2),
                        image: '/product_images/' +  product._id  + '/' +  product.image,
                    });
                } else {
                    let cart = req.session.cart;
                    let newItem = true;

                    for(let i=0; i< cart.length; i++){
                        if(cart[i].title == slug){
                            cart[i].qty++;
                            newItem = false;
                            break;
                        }
                    }

                    if(newItem){
                        cart.push({
                            title: slug,
                            qty: 1,
                            price: parseFloat(product.price).toFixed(2),
                            image: '/product_images/' +  product._id  + '/' +  product.image, 
                        });
                    }
                } 
                console.log(req.session.cart);
                req.flash('success_msg', "Product is added to cart successfully");
                res.redirect("back");                               
            }             
        });
    },

    checkout: (req, res) => {
        let cart = req.session.cart;

        if(cart && cart.length == 0){
            delete cart;
        } else {
            res.render('cart/checkout', {
                title: "Checkout",
                cart: cart
            });
        }
    },

    update: (req, res) => {
        let slug = req.params.product;
        let action = req.query.action;
        let cart = req.session.cart;

        for (let i = 0; i < cart.length; i++) {
            if(slug = cart[i].title){

                switch (action) {
                    case "increment":
                        cart[i].qty++;
                        break;
                    case "decrement":
                        cart[i].qty--;
                        if(cart[i].qty < 1){
                            cart.splice(i, 1);    
                        }   
                        break;
                    case "clear":
                        cart.splice(i, 1)
                        if(cart.length == 0)
                            delete req.session.cart
                        break;
                    default:
                        console.log("cart update problem")
                }

                break;
            }            
        }

        req.flash('success_msg', "Cart is updated successfully");
        res.redirect('/cart/checkout');
    },

    clear: (req, res) => {
        delete req.session.cart;
        
        req.flash('success_msg', "Cart is cleared successfully");
        res.redirect('/cart/checkout');        
    }
}