const fs = require("fs-extra");

const Page = require('../models/page.model');
const Product = require('../models/product.model');
const Category = require('../models/category.model');

module.exports = {

    index: (req, res) => {
      Page.findOne({slug: 'home'}, (err, page) => {
        if(err)
          return console.log(err);

        res.render('home/index', {
          title: page.title,
          content: page.content
        });         
      });
    },

    showPage: (req, res) => {
      let slug = req.params.slug;

      Page.findOne({slug: slug}, (err, page) => {
        if(err)
          return console.log(err);

        if(!page){
          res.redirect('/');
        } else {
          res.render('home/index', {
            title: page.title,
            content: page.content
          });
        }  
      });
    },

    showAllProducts: (req, res) => {
      let count;

      Product.count((err, c) => {
          count = c;
      });

      Product.find((err, products) => {
          res.render('home/products', {
              products: products,
              count: count,
              title:"All Products" 
          });
      });      
    },

    showProductsByCatgeory: (req, res) => {
      let categorySlug = req.params.category;

      Category.findOne({slug: categorySlug}, (err, cat) => {
        if(err){
          console.log(err)
        } else{
          Product.find({category: categorySlug}, (err, products) => {
            res.render('home/products', {
                products: products,
                title:"All Products" 
            });            
          });
        }
      })
    },

    showProductDetails: (req, res) => {
      // let categorySlug = req.params.category;
      let productSlug = req.params.product;
      let galleryImages = null;

      Product.findOne({slug: productSlug}, (err, product) => {
        if(err)
          return console.log(err);

        if(product){
          let galleryDir = "public/product_images/" + product._id + "/gallery";
        
          fs.readdir(galleryDir, (err, files) => {
            if(err){
              console.log(err);
            } else {
              galleryImages = files;
              res.render('home/product_details', {
                title: product.title,
                product: product,
                galleryImages: galleryImages 
              });
            }
          });          
        }  

      });
    }

};