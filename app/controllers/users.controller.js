const passport = require('passport');
const bcrypt = require('bcryptjs');

const User = require('../models/user.model');

module.exports = {

    signUp: (req, res) =>{
        res.render("users/sign_up", {
            title: "Sign Up" 
        });
    },

    postSignUp: (req, res) => {
        let name = req.body.name;
        let username = req.body.username;
        let email = req.body.email;
        let password = req.body.password;
        let confirm_password = req.body.confirm_password;

        req.checkBody('name', 'Fullname is required').notEmpty();
        req.checkBody('username', 'Username is required').notEmpty();
        req.checkBody('email', 'Email is required').isEmail();
        req.checkBody('password', 'Pasword is required').notEmpty();
        req.checkBody('confirm_password', 'Confirm Password is required').equals(password);

        let errors = req.validationErrors();

        if(errors){
            res.render("users/sign_up", {
                title: "Sign Up",
                errors: errors,
                name: name,
                username: username,
                email: email,
                user: null 
            });            
        } else {
            User.findOne({username: username}, (err, user) => {
                
                if(err)
                    return console.log(err);

                if(user){
                    req.flash('error_msg', "Username is alreday taken. Please try with another one");
                    res.render("users/sign_up", {
                        title: "Sign Up",
                        name: name,
                        username: username,
                        email: email 
                    });                     
                } else {
                    let user = new User({
                        name: name,
                        username: username,
                        email: email,
                        password: password,
                        admin: 0
                    });

                    bcrypt.genSalt(10, (err, salt) => {
                        bcrypt.hash(user.password, salt, (err, hash) => {
                            if(err)
                                return console.log(err);

                            user.password = hash;
                            user.save((err) => {
                                if(err)
                                    return console.log(err);

                                req.flash('success_msg', "You are successfully registered! Please sign in");
                                res.redirect('/users/sign_in');
                            });    
                        });
                    });
                }
            })
        }

    },

    signIn: (req, res) =>{

        if(res.locals.user)
            return res.redirect('/');

        res.render("users/sign_in", {
            title: "Sign In" 
        });
    }, 
    
    postSignIn: (req, res, next) => {
        
        passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/users/sign_in',
            failureFlash: true
        })(req, res, next);

    },

    signOut: (req, res) => {
        req.logout();
        req.flash('success_msg', "You are successfully logout!");
        res.redirect('/users/sign_in');
    }

}