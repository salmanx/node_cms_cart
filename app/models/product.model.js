const mongoose = require('mongoose');

const productModel = mongoose.Schema({
	title: { type: String, required: true },
    slug: { type: String },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    category: { type: String, required: true },
    image: { type: String }
});

module.exports = mongoose.model('Product', productModel);