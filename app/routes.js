const Home = require("./controllers/home.controller");
const Cart = require("./controllers/cart.controller");
const Page = require("./controllers/admin/pages.controller");
const Category = require("./controllers/admin/categories.controller");
const Product = require("./controllers/admin/products.controller");
const Users = require('./controllers/users.controller');
const Auth = require("./config/auth");

module.exports = function(app) {
    app.get('/', Home.index);
    app.get('/pages/:slug', Home.showPage);
    app.get('/products', Home.showAllProducts);
    app.get('/products/:category', Home.showProductsByCatgeory);
    app.get('/products/:category/:product', Home.showProductDetails);
    app.get('/cart/add/:slug', Auth.isUser, Cart.addProductToCart);
    app.get('/cart/checkout', Auth.isUser, Cart.checkout);
    app.get('/cart/update/:product', Auth.isUser, Cart.update);
    app.get('/cart/clear', Auth.isUser, Cart.clear)
    app.get('/users/sign_up', Users.signUp);
    app.post('/users/sign_up', Users.postSignUp);
    app.get('/users/sign_in', Users.signIn);
    app.post('/users/sign_in', Users.postSignIn);
    app.get('/logout', Users.signOut);

    // admin routes
    app.get('/admin/pages', Auth.isAdminUser, Page.index);
    app.get('/admin/pages/new', Auth.isAdminUser, Page.new);
    app.post('/admin/pages', Auth.isAdminUser, Page.create);    
    app.get('/admin/pages/edit/:id', Auth.isAdminUser, Page.edit);
    app.post('/admin/pages/update', Auth.isAdminUser, Page.update);
    app.get('/admin/pages/delete/:id', Auth.isAdminUser, Page.delete);
    app.post('/admin/pages/reorder-pages', Auth.isAdminUser, Page.reorderPages);

    app.get('/admin/categories', Auth.isAdminUser, Category.index);
    app.get('/admin/categories/new', Auth.isAdminUser, Category.new);
    app.post('/admin/categories', Auth.isAdminUser, Category.create);
    app.get('/admin/categories/edit/:id', Auth.isAdminUser, Category.edit);
    app.post('/admin/categories/update', Auth.isAdminUser, Category.update);
    app.get('/admin/categories/delete/:id', Auth.isAdminUser, Category.delete); 

    app.get('/admin/products', Auth.isAdminUser, Product.index);
    app.get('/admin/products/new', Auth.isAdminUser, Product.new);
    app.post('/admin/products', Auth.isAdminUser, Product.create);
    app.get('/admin/products/edit/:id', Auth.isAdminUser, Product.edit);
    app.post('/admin/products/update', Auth.isAdminUser, Product.update);
    app.get('/admin/products/delete/:id', Auth.isAdminUser, Product.delete);
    app.post('/admin/products/product-gallery/:id', Auth.isAdminUser, Product.uploadGalleryImages); 
    app.get('/admin/products/delete-image/:image', Auth.isAdminUser, Product.deleteGalleryimage);
};